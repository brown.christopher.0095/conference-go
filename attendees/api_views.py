import json
from django.http import JsonResponse
from common.json import ModelEncoder, LocationListEncoder, LocationDetailEncoder
from .models import Attendee
from django.views.decorators.http import require_http_methods
from events.models import Conference    

class AttendeeListEncoder(ModelEncoder):
    def default(self, o):
        if isinstance(o, Attendee):
            return {
                "id": o.id,
                "name": o.name,
                "email": o.email,
                "company_name": o.company_name,
                "conference_id": o.conference.id,
                "created": o.created.isoformat() if o.created else None
            }
        return super().default(o)

class AttendeeDetailEncoder(ModelEncoder):
    def default(self, o):
        if isinstance(o, Attendee):
            # Check if the attendee has a badge
            badge = getattr(o, 'badge', None)

            return {
                "id": o.id,
                "name": o.name,
                "email": o.email,
                "company_name": o.company_name,
                "conference_id": o.conference.id,
                "created": o.created.isoformat() if o.created else None,
                "badge": {
                    "created": badge.created.isoformat() if badge and badge.created else None
                } if badge else None
            }
        return super().default(o)

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:  # POST request
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )

def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.
    """ 
    attendee = Attendee.objects.get(id=id)
    conference = attendee.conference

    return JsonResponse(
        {
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created.isoformat() if attendee.created else None,
            "conference": {
                "name": conference.name,
                "href": conference.get_api_url(),
            },
        }
    )