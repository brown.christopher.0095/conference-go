import json
from django.http import JsonResponse
from common.json import ModelEncoder, LocationListEncoder, LocationDetailEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
from .acls import get_weather_conditions, get_picture_url






class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:  # POST request
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
    ]
    def get_extra_data(self, o):
        return {}



def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href, and the current weather conditions.
    """
    try:
        conference = Conference.objects.get(id=id)
    except Conference.DoesNotExist:
        return JsonResponse({'error': 'Conference not found'}, status=404)

    weather = get_weather_conditions(conference.location.city, conference.location.state)

    conference_data = {
        "conference": ConferenceDetailEncoder().default(conference),
        "weather": weather
    }

    return JsonResponse(conference_data, safe=False)


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room count",
        "created"
        "updated"
        " picture_url"
    ]

    def get_extra_data(self, o):
        return {} 
    
@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:  # Handle POST request
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
            get_picture_url(f'{content["city"]}')
            location = Location.objects.create(**content)
            return JsonResponse(
                location,
                encoder=LocationDetailEncoder,
                safe=False,
            )
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    try:
        location = Location.objects.get(id=id)
    except Location.DoesNotExist:
        return JsonResponse({'error': 'Location not found'}, status=404)

    if request.method == "GET":
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:  # PUT method logic
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        
    Location.objects.filter(id=id).update(**content)

    # copied from get detail
    location = Location.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )