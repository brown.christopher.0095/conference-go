# Generated by Django 4.0.3 on 2024-01-12 22:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_remove_location_picture_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='picture_url',
            field=models.URLField(blank=True, null=True),
        ),
    ]
