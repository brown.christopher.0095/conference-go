from django.shortcuts import render
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY



def get_picture_url(city, state):
    query = f"{city}
    url = f"https://api.pexels.com/v1/search?query={query}&per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        api_dict = response.json()
        return api_dict["photos"][0]["src"]["original"]}
    



def get_weather_conditions(city, state):

    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    geo_response = requests.get(geo_url)
    if geo_response.status_code == 200 and geo_response.json():
        latitude = geo_response.json()[0]["lat"]
        longitude = geo_response.json()[0]["lon"]


        weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
        weather_response = requests.get(weather_url)
        if weather_response.status_code == 200:
            weather_data = weather_response.json()
            return {
                "temp": weather_data["main"]["temp"],
                "description": weather_data["weather"][0]["description"]
            }
    return None     
