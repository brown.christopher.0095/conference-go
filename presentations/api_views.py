from django.http import JsonResponse, HttpResponse
from common.json import ModelEncoder
from .models import Presentation, Conference
from django.views.decorators.http import require_http_methods
import json


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = [
            {
                "title": p.title,
                "status": p.status.name,
                "href": p.get_api_url(),
            }
            for p in Presentation.objects.filter(conference=conference_id)
        ]
        return JsonResponse({"presentations": presentations})

    else:  # POST request
        content = json.loads(request.body)
        content["conference"] = Conference.objects.get(id=conference_id)
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):
    try:
        presentation = Presentation.objects.get(id=id)
    except Presentation.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == "GET":
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            setattr(presentation, key, value)
        presentation.save()
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        presentation.delete()
        return HttpResponse(status=204)  # No Content